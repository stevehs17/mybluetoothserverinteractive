package com.ssimon.mybluetoothserverinteractive;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "BluetoothServerInt";
    private BluetoothSocket mSocket;
    private int mCounter = 190;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        keepScreenOn();
        showToast(this, "Starting");
    }

    private void keepScreenOn() {
        View v = findViewById(android.R.id.content);
        v.setKeepScreenOn(true);
    }

    public void startBluetoothServer(View unused) {
        new StartServerTask().execute();
    }

    private class StartServerTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showToast(MainActivity.this, "Starting server...");
        }

        @Override
        protected Void doInBackground(Void...v) {
            startServer();
            return null;
        }

         @Override
        protected void onPostExecute(Void v) {
             super.onPostExecute(v);
             showToast(MainActivity.this, "Connected.");
        }
    }

    public void startServer() {
        final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
        BluetoothAdapter adapt = BluetoothAdapter.getDefaultAdapter();
        if (adapt == null)
            throw new IllegalStateException("Bluetooth not supported");
        if (!adapt.isEnabled()) {
            showToast(this, "Bluetooth not enabled");
            return;
        }
        try {
            BluetoothServerSocket serv = adapt.listenUsingInsecureRfcommWithServiceRecord("test server", uuid);
            mSocket = serv.accept();
            serv.close();
        } catch (IOException e) {
            Log.v(TAG, e.getMessage());
        }
    }

    public void writeTemperature(View unused) {
        if (mSocket == null) {
            showAlert(this, "can't write - no connection");
            return;
        }
        try {
            OutputStream o = mSocket.getOutputStream();
            if (++mCounter > 210)
                mCounter = 190;
            String msg = "tem" + Integer.toString(mCounter);
            o.write(msg.getBytes());
            o.flush();
         } catch (IOException e) {
            Log.v(TAG, e.getMessage());
        }
    }

    public void writeLeftBrewCycle(View unused) {
        if (mSocket == null) {
            showAlert(this, "can't write - no connection");
            Log.v(TAG, "can't write - no connection");
            return;
        }
        try {
            OutputStream o = mSocket.getOutputStream();
            String msg = "cycl14";
            o.write(msg.getBytes());
            o.flush();
            Log.v(TAG, "Output written from server: " + msg);
            waitToSend();

            msg = "cycl24";
            o.write(msg.getBytes());
            Log.v(TAG, "Output written from server: " + msg);
            waitToSend();

            msg = "cycl34";
            o.write(msg.getBytes());
            Log.v(TAG, "Output written from server: " + msg);
            waitToSend();

            msg = "cycl44";
            o.write(msg.getBytes());
            Log.v(TAG, "Output written from server: " + msg);
            waitToSend();

            msg = "acklok";
            o.write(msg.getBytes());
            Log.v(TAG, "Output written from server: " + msg);
            waitToSend();
        } catch (IOException e) {
            Log.v(TAG, e.getMessage());
        }
    }

    public void writeLeftBrewStatus(View unused) {
        if (mSocket == null) {
            showAlert(this, "can't write - no connection");
            return;
        }
        try {
            OutputStream o = mSocket.getOutputStream();
            String msg = "acklok";
            o.write(msg.getBytes());
            o.flush();
        } catch (IOException e) {
            Log.v(TAG, e.getMessage());
        }
    }


    public void readInput(View unused) {
        new BluetoothReader().execute();
    }

    public class BluetoothReader extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showToast(MainActivity.this, "About to read...");
        }

        @Override
        protected String doInBackground(Void...voids) {
            if (mSocket == null) {
                return "can't read - no connection";
            } else {
                try {
                    InputStream i = mSocket.getInputStream();
                    byte[] buf = new byte[256];
                    int bytes = i.read(buf);
                    return new String(buf, 0, bytes);
                } catch (IOException e) {
                    return e.getMessage();
                }
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showAlert(MainActivity.this, "Input read by server: " + s);
        }
    }


    public void closeSocket(View unused) {
        if (mSocket != null) {
            closeSilently(mSocket);
            mSocket = null;
        }
    }

    static private void closeSilently(BluetoothSocket sock) {
        try {
            sock.close();
        } catch (IOException e) {}
    }

    void showAlert(Context con, String msg) {
        AlertDialog ad = new AlertDialog.Builder(con).create();
        ad.setMessage(msg);
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int unused) {
                        dialogInterface.dismiss();
                    }
                });
        ad.show();
    }

    static void showToast(Context c, String msg) {
        Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
    }

    static private void waitToSend() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException unused) {}
    }
}